# chassis

## Purpose

This is an implementation of the chassis microservice design pattern:

> When you start the development of an application you often spend a significant amount of time putting in place the mechanisms to handle cross-cutting concerns. Examples of cross-cutting concern include:
> 
> * Externalized configuration - includes credentials, and network locations of external services such as databases and message brokers
> * Logging - configuring of a logging framework such as log4j or logback
> * Health checks - a url that a monitoring service can “ping” to determine the health of the application
> * Metrics - measurements that provide insight into what the application is doing and how it is performing
> * Distributed tracing - instrument services with code that assigns each external request an unique identifier that is passed between services.

#### more

* https://microservices.io/patterns/microservice-chassis.html

## Features

See each assembly's README for details on each feature. 

### Clean Architecture

This chassis uses a clean architecture approach with presentation, infrastructure, persistence, application and domain layers defined in their own assembles.

Defining each layer in its own assembly means you can extract or reuse layers without having to extract the code from a single assembly. E.g. you can implement a new presentation layer with gRPC or something without altering the application or domain projects.

#### more

* https://github.com/jasontaylordev/NorthwindTraders
* https://github.com/jasontaylordev/CleanArchitecture


### REST API presentation layer

* REST interface via ASP.&#xfeff;NET Core Web Application
* MediatR requests sent to application layer

### Application layer

* CQRS Command Query Responsibility Segregation using MediatR requests
* Request logging and FluentValidation using MediatR pipeline behaviours
* Automapper Entity<=>DTO mapping
* xUnit integration testing of MediatR request handlers with EF core in-memory database

### Domain layer

* DDD Aggregate root (Order) and entities (Order.OrderDetails)
* DDD value objects (User.Address)
* xUnit unit testing

### Persistence layer

* EF Core code first db migrations
  * EF core DDD value object implementation
* Repository pattern (IOrderRepository implementation)