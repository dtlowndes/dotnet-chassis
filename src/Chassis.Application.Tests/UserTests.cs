﻿using AutoMapper;
using Bogus;
using Chassis.Application.Common.Exceptions;
using Chassis.Application.Common.MappingProfile;
using Chassis.Application.Users.Commands.CreateUser;
using Chassis.Domain.Entities;
using Chassis.Persistence;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Chassis.Application.Tests
{
    public class UserTests
    {
        [Fact]
        public async Task ShouldCreateUser()
        {
            // arrange
            var options = new DbContextOptionsBuilder<ChassisDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            var configurationProvider = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });
            var mapper = configurationProvider.CreateMapper();
            var command = new Faker<CreateUserCommand>()
                .StrictMode(true)
                .RuleFor(x => x.FirstName, x => x.Person.FirstName)
                .RuleFor(x => x.LastName, x => x.Person.LastName)
                .RuleFor(x => x.Email, x=> x.Person.Email)
                .Generate();
             
            // act
            using (var context = new ChassisDbContext(options))
            {
                CreateUserCommandHandler handler = new CreateUserCommandHandler(context, mapper);
                var result = await handler.Handle(command, CancellationToken.None);
                User user = await context.Users.FirstOrDefaultAsync();
                // assert
                Assert.True(user.FirstName == command.FirstName);
                Assert.True(user.LastName == command.LastName);
                Assert.True(user.Id > 0);
                Assert.True(user.Id == result.Id);
                Assert.True(user.FirstName== result.FirstName);
                Assert.True(user.LastName == result.LastName);
                Assert.True(user.Address is null);
                Assert.True(result.Address is null);
            }

        }

        [Fact]
        public async Task ShouldRaiseUniqueEmailException()
        {
            // arrange
            var options = new DbContextOptionsBuilder<ChassisDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            var configurationProvider = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });
            var mapper = configurationProvider.CreateMapper();
            var commands = new Faker<CreateUserCommand>()
                .StrictMode(true)
                .RuleFor(x => x.FirstName, x => x.Person.FirstName)
                .RuleFor(x => x.LastName, x => x.Person.LastName)
                .RuleFor(x => x.Email, x => x.Person.Email)
                .Generate(2);
            commands[1].Email = commands[0].Email; // set second command email to the same as the first
            // act
            using (var context = new ChassisDbContext(options))
            {
                CreateUserCommandHandler handler = new CreateUserCommandHandler(context, mapper);
                await handler.Handle(commands[0], CancellationToken.None);
                // assert
                await Assert.ThrowsAsync<NonUniqueEmailException>(async () => await handler.Handle(commands[1], CancellationToken.None));
            }

        }
    }
}
