using AutoMapper;
using Bogus;
using Chassis.Application.Common.MappingProfile;
using Chassis.Application.Orders.Commands.CreateOrder;
using Chassis.Application.Orders.Commands.CreateOrderLine;
using Chassis.Application.Orders.CreateOrder.Commands;
using Chassis.Domain.Entities;
using Chassis.Persistence;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Chassis.Application.Tests
{
    public class OrderTests
    {

        [Fact]
        public async Task ShouldCreateOrder()
        {
            // arrange
            var options = new DbContextOptionsBuilder<ChassisDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            var configurationProvider = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });
            var mapper = configurationProvider.CreateMapper();
            var command = new CreateOrderCommand(); // don't need faker command has no propoerties

            // act
            using (var repository = new OrderRepository(new ChassisDbContext(options)))
            {
                CreateOrderCommandHandler handler = new CreateOrderCommandHandler(repository, mapper);
                var result = await handler.Handle(command, CancellationToken.None);
                Order order = repository.GetOrders().FirstOrDefault();
                // assert
                Assert.True(order is not null);
            }
        }

        [Fact]
        public async Task ShouldCreateOrderLine()
        {
            // arrange
            var options = new DbContextOptionsBuilder<ChassisDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            var configurationProvider = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });
            var mapper = configurationProvider.CreateMapper(); var command = new CreateOrderCommand(); // don't need faker command has no propoerties

            // act
            using (var repository = new OrderRepository(new ChassisDbContext(options)))
            {
                CreateOrderCommandHandler orderHandler = new CreateOrderCommandHandler(repository, mapper);
                var result = await orderHandler.Handle(command, CancellationToken.None);
                Order order = repository.GetOrders().FirstOrDefault();
                CreateOrderLineCommandHandler orderLineHandler = new CreateOrderLineCommandHandler(repository, mapper);
                var orderLineCommand = new Faker<CreateOrderLineCommand>()
                    .RuleFor(x => x.Quantity, x => x.Random.Number(1, 10))
                    .RuleFor(x => x.ProductName, x => x.Commerce.ProductName())
                    .RuleFor(x => x.UnitPrice, x => Decimal.Parse(x.Commerce.Price()))
                    .Generate();
                orderLineCommand.OrderId = 1; // the order we created 
                result = await orderLineHandler.Handle(orderLineCommand, CancellationToken.None);
                // assert
                Assert.True(result.OrderLines.Count == 1);
            }
        }

    }
}
