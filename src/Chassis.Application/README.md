﻿# Application layer

* Interfaces
* Models
* Logic
* Commands / Queries
* Validators
* Exceptions

## Features

### Dependency Injection extension

Services define in the application layer are registered at in the RESTApi startup through:

```
services.AddApplicationServices();
```

Which is defined in `src\Chassis.Application\DependencyInjection.cs`

### MediatR CQRS

* Command Query Responsibility Segregation
* Separate reads (queries) from writes (commands)
* Can maximise performance, scalability, and simplicity
* Easy to add new features, just add a new query or command
* Easy to maintain, changes only affect one command or query
* Define commands and queries as requests
* Application layer is just a series of request/response objects
* Ability to attach additional behaviour before and/or after each request, e.g. logging, validation, caching,
authorisation and so on

We use [MediatR](https://github.com/jbogard/MediatR) in the application layer to declare and handle commands and queries (CQRS) for the presentation layer to use. 

This is an alternative approach to defining interfaces in this layer for the presentation layer to implement.

MediatR is added to our services via [MediatR.Extensions.Microsoft.DependencyInjection](https://github.com/jbogard/MediatR.Extensions.Microsoft.DependencyInjection).

#### MediatR pipeline behaviours

MediatR supports pipeline behaviours: https://github.com/jbogard/MediatR/wiki/Behaviors

We have leveraged this to add logging and validation to the application layer.

##### Logging

Logging behaviour has been definied in `RequestLoggingBehaviour`.

```
services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestLoggingBehaviour<,>));
```

##### FluentValidation

https://docs.fluentvalidation.net/en/latest/

We have added a generic pipeline behaviour to execute fluent validation across declared classes.

```
services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehaviour<,>));
```

The above pipeline will load all validators and execute during the pipeline.

To register all validators:

```
services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
```

#### Command handlers

We've implemented command handlers outside the command class in case the command needs to be serialised. One scenario that might benefit from this would be calling use cases from a different process, i.e. physically separating the core from outer layers, serialising the command class and providing that to an interface adapter. This might suit multi-tenant situations where the dependencies and data stores are configured differently for each customer but the domain application use cases and domain entities remain the same.