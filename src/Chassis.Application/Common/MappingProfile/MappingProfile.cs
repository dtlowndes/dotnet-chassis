﻿using AutoMapper;

namespace Chassis.Application.Common.MappingProfile
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // be explicit about the entities we're mapping
            CreateMap<Domain.Entities.User, Users.Models.UserDTO>();
            CreateMap<Domain.ValueObjects.Address, Users.Models.AddressDTO>();
            CreateMap<Domain.Entities.Order, Orders.Models.OrderDTO>();
            CreateMap<Domain.Entities.OrderLine, Orders.Models.OrderLineDTO>();
        }
    }
}
