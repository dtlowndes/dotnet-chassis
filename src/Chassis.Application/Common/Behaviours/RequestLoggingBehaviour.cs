﻿using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace Chassis.Application.Common.Behaviours
{
    public class RequestLoggingBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {

        private readonly ILogger<RequestLoggingBehaviour<TRequest,TResponse>> _logger;

        public RequestLoggingBehaviour(ILogger<RequestLoggingBehaviour<TRequest, TResponse>> logger)
        {
            _logger = logger;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            _logger.LogInformation($"Request: {typeof(TRequest).Name}");
            var response = await next();
            _logger.LogInformation($"Response: {typeof(TResponse).Name}");

            return response;
        }

    }
}
