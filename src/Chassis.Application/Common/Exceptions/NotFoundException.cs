﻿using System;

namespace Chassis.Application.Common.Exceptions
{
    public class NotFoundException : Exception
    {
        // credit https://github.com/jasontaylordev/NorthwindTraders
        public NotFoundException(string name, object key)
            : base($"Entity \"{name}\" ({key}) was not found.")
        {
        }
    }
}

