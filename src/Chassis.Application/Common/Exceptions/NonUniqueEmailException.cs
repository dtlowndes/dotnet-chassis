﻿using System;

namespace Chassis.Application.Common.Exceptions
{
    public class NonUniqueEmailException : Exception
    {
        // This exception currently exists in the application for simplicity
        // Some DDD proponents suggest this needs to be in the domain, Google "DDD unique set validation"
        public NonUniqueEmailException() : base("Email already exists.")
        { }

    }
}
