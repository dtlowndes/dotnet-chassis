﻿using Chassis.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Chassis.Application.Common.Interfaces
{
    /// <summary>
    /// Classes in outer layers implement this class to pass in an EF DBContext to the core.
    /// You can limit the abilities of outer classes by limiting this interface.
    /// An alternative is to decouple EF from the core using a more abstract repository interface as demonstrated 
    /// in the Orders use cases. This allows you flexibility to shift the repository from a relational database 
    /// to a NoSQL database for instance.
    /// </summary>
    public interface IChassisDbContext
    {
        public DbSet<User> Users { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        public bool UserEmailIsUnique(string email);

    }
}
