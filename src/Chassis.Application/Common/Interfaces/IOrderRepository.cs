﻿using Chassis.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Chassis.Application.Common.Interfaces
{
    // this interface doesn't depend on EF core, in contrast to IChassisDbContext
    // the benefit is a decoupling of EF from the core, with the tradeoff of more maintenance overhead
    // in some cases you may want to start with this until you've made a firm decision on EF or not
    // (one project I worked on switched from EF to NoSQL part way through)
    // and if EF is established you could refactor out this interface and just use a DbContext directly that already implements
    // the repository and unit of work patterns
    public interface IOrderRepository : IDisposable
    {
        IEnumerable<Order> GetOrders();
        Order GetOrderByID(int orderId);
        void InsertOrder(Order order);
        void DeleteOrder(int orderID);
        void UpdateOrder(Order order);
        Task<int> Save(CancellationToken cancellationToken); // we still want avoid blocking the thread during I/O operations
    }
}
