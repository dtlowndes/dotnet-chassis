﻿using Chassis.Application.Common.Behaviours;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Chassis.Application
{
    public static class DependencyInjection
    {
        // extension method called by the RESTAPI application to add these services on Startup
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            // Add Fluent Validation validators (implement AbstractValidator<request>) in this assembly
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            // Add MediatR service
            services.AddMediatR(Assembly.GetExecutingAssembly());
            // Add MediatR pipeline behaviour that will validate the request using registered validators for each request
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehaviour<,>));
            // Add MediatR pipeline behaviour to log each request
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestLoggingBehaviour<,>));
            // Add AutoMapper profiles (implement Profile) from this assembly
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            return services;
        }
    }
}
