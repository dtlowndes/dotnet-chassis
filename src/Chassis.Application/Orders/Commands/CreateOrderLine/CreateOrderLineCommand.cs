﻿using Chassis.Application.Orders.Models;
using MediatR;

namespace Chassis.Application.Orders.Commands.CreateOrderLine
{
    public class CreateOrderLineCommand : IRequest<OrderDTO>
    {
        public int Quantity { get; set; }
        public string ProductName { get; set; }
        public decimal UnitPrice { get; set; }
        public int OrderId { get; set; }
    }
}
