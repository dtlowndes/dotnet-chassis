﻿using FluentValidation;

namespace Chassis.Application.Orders.Commands.CreateOrderLine
{

    public class CreateOrderLineCommandValidator : AbstractValidator<CreateOrderLineCommand>
    {
        public CreateOrderLineCommandValidator()
        {
            RuleFor(x => x.Quantity).GreaterThanOrEqualTo(1).LessThanOrEqualTo(10);
        }
    }
}
