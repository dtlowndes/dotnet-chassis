﻿using AutoMapper;
using Chassis.Application.Common.Exceptions;
using Chassis.Application.Common.Interfaces;
using Chassis.Application.Orders.Models;
using Chassis.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Chassis.Application.Orders.Commands.CreateOrderLine
{
    public class CreateOrderLineCommandHandler : IRequestHandler<CreateOrderLineCommand, OrderDTO>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;

        public CreateOrderLineCommandHandler(IOrderRepository orderRepository, IMapper mapper)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
        }
        public async Task<OrderDTO> Handle(CreateOrderLineCommand request, CancellationToken cancellationToken)
        {
            Order order = _orderRepository.GetOrderByID(request.OrderId);
            if (order is null) throw new NotFoundException(nameof(Order), request.OrderId);

            order.AddOrderLine(request.Quantity, request.ProductName, request.UnitPrice);
            await _orderRepository.Save(cancellationToken);
            OrderDTO vm = _mapper.Map<Order, OrderDTO>(order);
            return vm;
        }
    }
}
