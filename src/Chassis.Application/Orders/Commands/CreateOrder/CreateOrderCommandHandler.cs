﻿using AutoMapper;
using Chassis.Application.Common.Interfaces;
using Chassis.Application.Orders.CreateOrder.Commands;
using Chassis.Application.Orders.Models;
using Chassis.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Chassis.Application.Orders.Commands.CreateOrder
{
    public class CreateOrderCommandHandler : IRequestHandler<CreateOrderCommand, OrderDTO>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;

        public CreateOrderCommandHandler(IOrderRepository orderRepository, IMapper mapper)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
        }
        public async Task<OrderDTO> Handle(CreateOrderCommand request, CancellationToken cancellationToken)
        {
            Order order = new Order();
            _orderRepository.InsertOrder(order);
            await _orderRepository.Save(cancellationToken);
            OrderDTO vm = _mapper.Map<Order, OrderDTO>(order);
            return vm;
        }
    }
}
