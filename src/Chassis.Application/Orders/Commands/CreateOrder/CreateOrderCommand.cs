﻿using Chassis.Application.Orders.Models;
using MediatR;

namespace Chassis.Application.Orders.CreateOrder.Commands
{
    public class CreateOrderCommand : IRequest<OrderDTO>
    {
        //nothing here as Id and OrderDate are generated in the entity
    }
}
