﻿using Chassis.Application.Orders.Models;
using MediatR;

namespace Chassis.Application.Orders.Queries
{
    public class GetOrderQuery : IRequest<OrderDTO>
    {
        public int OrderId { get; set; }
    }
}
