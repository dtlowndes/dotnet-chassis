﻿using AutoMapper;
using Chassis.Application.Common.Interfaces;
using Chassis.Application.Orders.Models;
using Chassis.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Chassis.Application.Orders.Queries
{
    public class GetOrderQueryHandler : IRequestHandler<GetOrderQuery, OrderDTO>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;

        public GetOrderQueryHandler(IOrderRepository orderRepository, IMapper mapper)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
        }

        public async Task<OrderDTO> Handle(GetOrderQuery request, CancellationToken cancellationToken)
        {
            var order = _orderRepository.GetOrderByID(request.OrderId);
            OrderDTO vm = _mapper.Map<Order, OrderDTO>(order);
            return vm;
        }
    }
}
