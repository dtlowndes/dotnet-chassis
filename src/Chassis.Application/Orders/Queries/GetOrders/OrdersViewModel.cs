﻿using Chassis.Application.Orders.Models;
using System.Collections.Generic;

namespace Chassis.Application.Users.Queries.GetOrders
{
    class OrdersViewModel
    {
        public IList<OrderDTO> Orders { get; set; }
    }
}
