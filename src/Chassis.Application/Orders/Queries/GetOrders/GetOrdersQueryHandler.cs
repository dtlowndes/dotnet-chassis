﻿using AutoMapper;
using Chassis.Application.Common.Interfaces;
using Chassis.Application.Orders.Models;
using Chassis.Domain.Entities;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Chassis.Application.Users.Queries.GetOrders
{
    class GetOrdersQueryHandler : IRequestHandler<GetOrdersQuery, OrdersViewModel>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;

        public GetOrdersQueryHandler(IOrderRepository orderRepository, IMapper mapper)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
        }

        public async Task<OrdersViewModel> Handle(GetOrdersQuery request, CancellationToken cancellationToken)
        {
            var vm = new OrdersViewModel
            {
                Orders = _mapper.Map<List<Order>, List<OrderDTO>>(_orderRepository.GetOrders().ToList())
            };

            return vm;
        }
    }
}
