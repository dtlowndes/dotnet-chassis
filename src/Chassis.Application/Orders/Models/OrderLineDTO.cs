﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chassis.Application.Orders.Models
{
    public class OrderLineDTO
    {
        public int Quantity { get; set; }
        public string ProductName { get; set; }
        public decimal UnitPrice { get; set; }
    }
}
