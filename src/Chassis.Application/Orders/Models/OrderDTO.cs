﻿using System;
using System.Collections.Generic;

namespace Chassis.Application.Orders.Models
{
    public class OrderDTO
    {
        public int Id { get; set; }
        public DateTime OrderDate { get; set; }
        public List<OrderLineDTO> OrderLines { get; private set; }
    }
}
