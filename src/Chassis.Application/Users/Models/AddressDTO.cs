﻿namespace Chassis.Application.Users.Models
{
    public class AddressDTO
    {
        public string Street { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
    }
}
