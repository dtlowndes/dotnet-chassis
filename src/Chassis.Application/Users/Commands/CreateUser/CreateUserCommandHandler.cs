﻿using AutoMapper;
using Chassis.Application.Common.Exceptions;
using Chassis.Application.Common.Interfaces;
using Chassis.Application.Users.Models;
using Chassis.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Chassis.Application.Users.Commands.CreateUser
{
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, UserDTO>
    {
        private readonly IChassisDbContext _context;
        private readonly IMapper _mapper;

        public CreateUserCommandHandler(IChassisDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<UserDTO> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            if (!_context.UserEmailIsUnique(request.Email)) throw new NonUniqueEmailException();

            var user = new User
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };
            _context.Users.Add(user);
            await _context.SaveChangesAsync(cancellationToken);
            UserDTO vm = _mapper.Map<User, UserDTO>(user);
            return vm;
        }
    }
}
