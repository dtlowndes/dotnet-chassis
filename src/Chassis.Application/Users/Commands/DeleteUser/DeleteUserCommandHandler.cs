﻿using AutoMapper;
using Chassis.Application.Common.Exceptions;
using Chassis.Application.Common.Interfaces;
using Chassis.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Chassis.Application.Users.Commands.DeleteUser
{
    public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand>
    {
        private readonly IChassisDbContext _context;
        private readonly IMapper _mapper;

        public DeleteUserCommandHandler(IChassisDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<Unit> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            User user = _context.Users.Find(request.Id);
            if (user is null) throw new NotFoundException(nameof(User), request.Id);

            _context.Users.Remove(user);
            await _context.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
