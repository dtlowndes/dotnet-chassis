﻿using Chassis.Application.Users.Models;
using MediatR;

namespace Chassis.Application.Users.Commands.CreateUserAddress
{
    public class CreateUserAddressCommand : IRequest<UserDTO>
    {
        public int UserId { get; set; }
        public string Street { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
    }
}
