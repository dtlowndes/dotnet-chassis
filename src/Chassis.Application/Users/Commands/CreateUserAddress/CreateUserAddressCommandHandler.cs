﻿using AutoMapper;
using Chassis.Application.Common.Exceptions;
using Chassis.Application.Common.Interfaces;
using Chassis.Application.Users.Models;
using Chassis.Domain.Entities;
using Chassis.Domain.ValueObjects;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Chassis.Application.Users.Commands.CreateUserAddress
{
    public class CreateUserAddressCommandHandler : IRequestHandler<CreateUserAddressCommand, UserDTO>
    {
        private readonly IChassisDbContext _context;
        private readonly IMapper _mapper;

        public CreateUserAddressCommandHandler(IChassisDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<UserDTO> Handle(CreateUserAddressCommand request, CancellationToken cancellationToken)
        {
            User user = _context.Users.Find(request.UserId);
            if (user is null) throw new NotFoundException(nameof(User), request.UserId);

            user.SetAddress(new Address(
                request.Street,
                request.City,
                request.PostCode));
            await _context.SaveChangesAsync(cancellationToken);
            UserDTO vm = _mapper.Map<User, UserDTO>(user);
            return vm;
        }
    }
}
