﻿using FluentValidation;

namespace Chassis.Application.Users.Commands.CreateUserAddress
{
    public class CreateUserAddressCommandValidator : AbstractValidator<CreateUserAddressCommand>
    {
        public CreateUserAddressCommandValidator()
        {
            RuleFor(x => x.Street).MaximumLength(256).NotEmpty();
            RuleFor(x => x.PostCode).NotEmpty().Matches("^[0-9]{4}$").WithMessage("PostCode must be in the format 0000");
            RuleFor(x => x.City).MaximumLength(256).NotEmpty();
        }
    }
}
