﻿using Chassis.Application.Users.Models;
using System.Collections.Generic;

namespace Chassis.Application.Users.Queries.GetUsers
{
    class UsersViewModel
    {
        public IList<UserDTO> Users { get; set; }
    }
}
