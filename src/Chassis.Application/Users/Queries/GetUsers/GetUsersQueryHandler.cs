﻿using AutoMapper;
using Chassis.Application.Common.Interfaces;
using Chassis.Application.Users.Models;
using Chassis.Domain.Entities;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Chassis.Application.Users.Queries.GetUsers
{
    class GetUsersQueryHandler : IRequestHandler<GetUsersQuery, UsersViewModel>
    {
        private readonly IChassisDbContext _context;
        private readonly IMapper _mapper;

        public GetUsersQueryHandler(IChassisDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<UsersViewModel> Handle(GetUsersQuery request, CancellationToken cancellationToken)
        {
            var vm = new UsersViewModel
            {
                Users = _mapper.Map<List<User>, List<UserDTO>>(_context.Users.ToList())
            };

            return vm;
        }
    }
}
