﻿using AutoMapper;
using Chassis.Application.Common.Interfaces;
using Chassis.Application.Users.Models;
using Chassis.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Chassis.Application.Users.Queries.GetUser
{
    class GetUserQueryHandler : IRequestHandler<GetUserQuery, UserDTO>
    {
        private readonly IChassisDbContext _context;
        private readonly IMapper _mapper;

        public GetUserQueryHandler(IChassisDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<UserDTO> Handle(GetUserQuery request, CancellationToken cancellationToken)
        {
            var user = _context.Users.Find(request.UserId);
            UserDTO vm = _mapper.Map<User, UserDTO>(user);
            return vm;
        }
    }
}
