﻿using Chassis.Application.Users.Models;
using MediatR;

namespace Chassis.Application.Users.Queries.GetUser
{
    public class GetUserQuery : IRequest<UserDTO>
    {
        public int UserId { get; set; }
    }
}
