﻿using Chassis.Application.Common.Interfaces;
using Chassis.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Chassis.Persistence
{
    // this class follows the example at https://docs.microsoft.com/en-us/aspnet/mvc/overview/older-versions/getting-started-with-ef-5-using-mvc-4/implementing-the-repository-and-unit-of-work-patterns-in-an-asp-net-mvc-application#creating-the-Order-repository-class
    public class OrderRepository : IOrderRepository, IDisposable
    {
        private readonly ChassisDbContext _context;

        public OrderRepository(ChassisDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Order> GetOrders()
        {
            return _context.Orders.Include(o => o.OrderLines);
        }

        public Order GetOrderByID(int id)
        {
            return _context.Orders.Where(o => o.Id == id).Include(o => o.OrderLines).FirstOrDefault();
        }

        public void InsertOrder(Order order)
        {
            _context.Orders.Add(order);
        }

        public void DeleteOrder(int orderID)
        {
            Order order = _context.Orders.Find(orderID);
            _context.Orders.Remove(order);
        }

        public void UpdateOrder(Order order)
        {
            _context.Entry(order).State = EntityState.Modified;
        }

        public async Task<int> Save(CancellationToken cancellationToken)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
