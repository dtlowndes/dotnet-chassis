﻿using Chassis.Application.Common.Interfaces;
using Chassis.Domain.Entities;
using Chassis.Persistence.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System.Linq;

namespace Chassis.Persistence
{
    public class ChassisDbContext : DbContext, IChassisDbContext
    {
        public ChassisDbContext(DbContextOptions<ChassisDbContext> options) : base(options)
        {
        }

        // this is required for migrations since they run against this class library project only
        // pass the connection string to migrations via
        // `dotnet ef database update --connection "Host=localhost;Database=chassis;Username=postgres;Password=password"`
        // at runtime, Startup calls AddPersistence extension method to establsh the the runtime database connection
        public class ChassisDbContextFactory : IDesignTimeDbContextFactory<ChassisDbContext>
        {
            public ChassisDbContext CreateDbContext(string[] args)
            {
                var optionsBuilder = new DbContextOptionsBuilder<ChassisDbContext>();
                optionsBuilder.UseNpgsql();

                return new ChassisDbContext(optionsBuilder.Options);
            }
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }

        public bool UserEmailIsUnique(string email)
        {
            var user = Users.FirstOrDefault(u => u.Email == email);
            return (user is null);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // To reduce the size of the OnModelCreating method all configuration for an entity type
            // can be extracted to a separate class implementing IEntityTypeConfiguration<TEntity>.
            // https://docs.microsoft.com/en-us/ef/core/modeling/#grouping-configuration
            // Below, we're configuring the User entity via UserConfiguration
            builder.ApplyConfiguration(new UserConfiguration());
            // or
            // builder.ApplyConfigurationsFromAssembly(typeof(ChassisDbContext).Assembly);
        }

    }
}
