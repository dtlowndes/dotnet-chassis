﻿using Chassis.Application.Common.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Chassis.Persistence
{
    public static class DependencyInjection
    {
        // extension method called by the RESTAPI application to add these services on Startup
        public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ChassisDbContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"))
            );
            services.AddScoped<IChassisDbContext>(provider => provider.GetService<ChassisDbContext>());
            services.AddScoped<IOrderRepository, OrderRepository>();

            return services;
        }
    }
}
