﻿# Chasses persistence layer

## Setup

- Install Entity Framework for .NET Core: `Install-Package Microsoft.EntityFrameworkCore`
- Create the DbContext, see (ChassisDbContext.cs)[ChassisDbContext.cs]
  - A DbContext instance is designed to be used for a single (unit-of-work)[https://www.martinfowler.com/eaaCatalog/unitOfWork.html]. This means that the lifetime of a DbContext instance is usually very short.
  - We don't implement the clean architecture repository pattern as DbContext does this for us and is already an abstraction, we're unlikely to switch from EF Core to another ORM and we can swap out the provider in EF Core to change the (relational) database engine.
  - We do however implement an interface (IChassisDbContext) in the Application layer to ensure we maintain clean architecture principle of no dependencies on outer layers, this interface also allows us to restrict what the application layer can do, e.g. only `SaveChangesAsync()`
- The run-time connection string is provided via DI configuration in `src\Chassis.Persistence\DependencyInjection.cs`
- The design-time (migrations) connection string is provided by implementing `IDesignTimeDbContextFactory` and providing the connection string on the command line (see migrations below)
- Create configurations for the DbContext, e.g. you need explictly declare Value Object relations as 'owned types are never discovered by convention in EF Core'
  - https://docs.microsoft.com/en-us/dotnet/architecture/microservices/microservice-ddd-cqrs-patterns/implement-value-objects#persist-value-objects-as-owned-entity-types-in-ef-core-20-and-later

## Fluent configuration

To reduce the size of the OnModelCreating method all configuration for an entity type can be extracted to a separate class implementing IEntityTypeConfiguration\<TEntity\>. In this project we're configuring the `User` entity via `UserConfiguration`. We use Fluent configuration for `User` as per https://docs.microsoft.com/en-us/ef/core/modeling/#use-fluent-api-to-configure-a-model.

## EF Core code first migrations

Install the .NET Core CLI tools: `dotnet tool install --global dotnet-ef`.

Running the code without migrations will result in `42P01: relation "Users" does not exist` error.

To support migrations outside docker and without worrying about the startup project we implement `IDesignTimeDbContextFactory<ChassisDbContext>`, see (ChassisDbContext.cs)[ChassisDbContext.cs].

- Create the initial migration: `dotnet ef migrations add InitialCreate`
- Run the migration from the persistence project:
```
cd .\src\Chassis.Persistence\
dotnet ef database update --connection "Host=localhost;Database=chassis;Username=postgres;Password=password"
```