using Chassis.Domain.Entities;
using Xunit;
using System;
using System.Linq;

namespace Chassis.Domain.Tests
{
    public class OrderTests
    {
        [Fact]
        public void ShouldCreateOrderLine()
        {
            //arrange
            var order = new Order();
            //act
            order.AddOrderLine(5, "Bones", 0.1m);
            var orderLine = order.OrderLines.First();
            //assert
            Assert.True(orderLine.Quantity == 5);
            Assert.True(orderLine.ProductName == "Bones");
            Assert.True(orderLine.UnitPrice == 0.1m);
        }

        [Fact]
        public void ShouldCalculateOrderLineTotal()
        {
            //arrange
            var order = new Order();
            //act
            order.AddOrderLine(5, "Bones", 0.1m);
            var orderLine = order.OrderLines.First();
            //assert
            Assert.True(orderLine.Total == 0.5m);
        }

        [Fact]
        public void ShouldAddOrderLines()
        {
            //arrange
            var order = new Order();
            //act
            order.AddOrderLine(5, "Bones", 0.1m); // 0.5
            order.AddOrderLine(2, "Knives", 0.5m); // 1.0
            order.AddOrderLine(1, "Shovels", 0.75m); // 0.75
            //assert
            Assert.True(order.OrderLines.Count == 3);
        }

        [Fact]
        public void ShouldCalculateOrderTotal()
        {
            //arrange
            var order = new Order();
            //act
            order.AddOrderLine(5, "Bones", 0.1m); // 0.5
            order.AddOrderLine(2, "Knives", 0.5m); // 1.0
            order.AddOrderLine(1, "Shovels", 0.75m); // 0.75
            //assert
            Assert.True(order.Total == 2.25m);
        }

        [Fact]
        public void ShouldCreateOrderDate()
        {
            //arrange
            //act
            var order = new Order();
            //assert
            Assert.Equal(1, DateTime.Compare(DateTime.UtcNow, order.OrderDate)); // new DateTime is more recent than OrderDate
            Assert.True(order.OrderDate > DateTime.MinValue.ToUniversalTime()); // OrderDate is greater than new DateTime with minimum value
        }
    }
}
