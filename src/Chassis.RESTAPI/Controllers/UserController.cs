﻿using Chassis.Application.Common.Exceptions;
using Chassis.Application.Users.Commands.CreateUser;
using Chassis.Application.Users.Commands.CreateUserAddress;
using Chassis.Application.Users.Commands.DeleteUser;
using Chassis.Application.Users.Models;
using Chassis.Application.Users.Queries.GetUser;
using Chassis.Application.Users.Queries.GetUsers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Chassis.WebAPI.Controllers
{
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Route("user")]
    public class UserController : Controller
    {

        private readonly IMediator _mediator;

        public UserController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult<UserDTO>> Create(CreateUserCommand command)
        {
            try
            {
                var vm = await _mediator.Send(command);
                return CreatedAtAction(nameof(Read), new { id = vm.Id }, vm);
            }
            catch (NonUniqueEmailException e)
            {
                return BadRequest(new { error = e.Message });
            }
        }

        [HttpGet]
        public async Task<ActionResult<string>> List()
        {
            var vm = await _mediator.Send(new GetUsersQuery());
            return Ok(vm);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> Read(int id)
        {
            var query = new GetUserQuery
            {
                UserId = id
            };
            var vm = await _mediator.Send(query);
            if (vm == null) return NotFound();
            return Ok(vm);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            DeleteUserCommand command = new DeleteUserCommand
            {
                Id = id
            };
            try
            {
                await _mediator.Send(command);
                return NoContent();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Route("{id}/address")]
        public async Task<ActionResult<UserDTO>> CreateAddress(int id, CreateUserAddressCommand command)
        {
            command.UserId = id;
            try
            {
                var vm = await _mediator.Send(command);
                return CreatedAtAction(nameof(Read), new { id = vm.Id }, vm);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }
    }
}
