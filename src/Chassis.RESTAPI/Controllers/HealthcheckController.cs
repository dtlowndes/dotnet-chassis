﻿using Microsoft.AspNetCore.Mvc;

namespace Chassis.RESTAPI.Controllers
{
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Route("healthcheck")]
    public class HealthcheckController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return Ok("OK");
        }
    }
}
