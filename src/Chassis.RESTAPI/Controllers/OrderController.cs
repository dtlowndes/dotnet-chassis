﻿using Chassis.Application.Orders.Commands.CreateOrderLine;
using Chassis.Application.Orders.CreateOrder.Commands;
using Chassis.Application.Orders.Models;
using Chassis.Application.Orders.Queries;
using Chassis.Application.Users.Queries.GetOrders;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Chassis.WebAPI.Controllers
{
    [ApiController]
    [Route("order")]
    public class OrderController : Controller
    {
        public IMediator _mediator { get; set; }

        public OrderController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult<OrderDTO>> Create(CreateOrderCommand command)
        {
            var vm = await _mediator.Send(command);
            return CreatedAtAction(nameof(Read), new { id = vm.Id }, vm);
        }


        [HttpGet]
        public async Task<ActionResult<string>> List()
        {
            var vm = await _mediator.Send(new GetOrdersQuery());
            return Ok(vm);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<OrderDTO>> Read(int id)
        {
            var request = new GetOrderQuery
            {
                OrderId = id
            };
            var vm = await _mediator.Send(request);
            if (vm == null) return NotFound();
            return Ok(vm);
        }

        [HttpPost]
        [Route("{id}/orderline")]
        public async Task<ActionResult<OrderDTO>> CreateOrderLine(int id, CreateOrderLineCommand command)
        {
            command.OrderId = id;
            var vm = await _mediator.Send(command);
            return CreatedAtAction(nameof(Read), new { id = vm.Id }, vm);
        }
    }
}
