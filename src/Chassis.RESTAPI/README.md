﻿# REST API

Basic REST API presentation layer.

## Foundation

1. Visual Studio, start new project
1. ASP.&#xfeff;NET Core Web Application
1. ASP.&#xfeff;NET Core Web API template

The default weather controller and class were then removed.

### more

* https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/web-host?view=aspnetcore-5.0

## Features

### Integration to the application layer

The application layer is used by the presentation layer to execute its use cases. Services define in the application layer are registered at startup through:

```
services.AddApplicationServices();
```

Which is defined in `src\Chassis.Application\DependencyInjection.cs`

### Logging

https://docs.microsoft.com/en-us/aspnet/core/fundamentals/logging/?view=aspnetcore-5.0

[Serilog](https://serilog.net/) has been installed via:

```
dotnet add chassis package Serilog.AspNetCore
```

See https://github.com/serilog/serilog-aspnetcore for instructions including changes to `Program.Main` to configure.

```csharp
public static IHostBuilder CreateHostBuilder(string[] args) =>
    Host.CreateDefaultBuilder(args)
        .UseSerilog() // serilog added here
        .ConfigureWebHostDefaults(webBuilder =>
        {
            webBuilder.UseStartup<Startup>();
        });
```

`Logging` has been removed from `appsettings.json`.

There is actually no logging (yet) in this assembly. There is logging into the application layer, see the application assembly README.
