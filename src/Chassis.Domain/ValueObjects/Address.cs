﻿using System;

namespace Chassis.Domain.ValueObjects
{
    public class Address
    {
        public String Street { get; private set; }
        public String City { get; private set; }
        public String PostCode { get; private set; }

        public Address() { }

        public Address(string street, string city, string postcode)
        {
            Street = street;
            City = city;
            PostCode = postcode;
        }
    }
}