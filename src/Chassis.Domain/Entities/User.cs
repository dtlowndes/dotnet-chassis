﻿using Chassis.Domain.Common;
using Chassis.Domain.ValueObjects;

namespace Chassis.Domain.Entities
{
    public class User : Entity
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Address Address { get; private set; }

        public override string ToString()
        {
            return $"{FirstName} {LastName}";
        }

        public void SetAddress(Address address)
        {
            Address = address;
        }

    }
}
