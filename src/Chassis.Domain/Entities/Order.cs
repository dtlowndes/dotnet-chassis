﻿using Chassis.Domain.Common;
using Chassis.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Chassis.Domain.Entities
{
    public class Order : Entity
    {
        public const int MAX_PRODUCTS = 5;

        public Order()
        {
            OrderLines = new List<OrderLine>();
            OrderDate = DateTime.UtcNow;
        }

        public DateTime OrderDate { get; private set; }
        public ICollection<OrderLine> OrderLines { get; private set; }

        public decimal Total { get; private set; }

        public void AddOrderLine(int quantity, string productName, decimal unitPrice)
        {
            if (OrderLines.FirstOrDefault(ol => ol.ProductName == productName) != null)
                throw new DuplicateProductException(productName);

            if (OrderLines.Count == MAX_PRODUCTS)
                throw new MaximumProductsException(MAX_PRODUCTS);

            var orderLine = new OrderLine()
            {
                Quantity = quantity,
                ProductName = productName,
                UnitPrice = unitPrice
            };
            OrderLines.Add(orderLine);
            RecalculateTotal();
        }

        private void RecalculateTotal()
        {
            decimal total = 0;
            foreach (OrderLine orderLine in OrderLines)
            {
                total += orderLine.Total;
            }
            Total = total;
        }
    }
}
