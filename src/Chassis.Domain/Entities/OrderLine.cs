﻿using Chassis.Domain.Common;

namespace Chassis.Domain.Entities
{
    public class OrderLine : Entity
    {
        public int Quantity { get; set; }
        public string ProductName { get; set; }
        public decimal UnitPrice { get; set; }
        public Order Order { get; private set; }

        public decimal Total
        {
            get { return Quantity * UnitPrice; }
        }
    }
}
