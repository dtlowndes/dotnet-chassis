﻿namespace Chassis.Domain.Common
{
    public abstract class Entity
    {
        public int Id { get; private set; }
    }
}
