﻿using System;

namespace Chassis.Domain.Exceptions
{
    public class MaximumProductsException : Exception
    {
        public MaximumProductsException(int max)
            : base($"Limit of {max} products in this order.")
        {
        }
    }
}
