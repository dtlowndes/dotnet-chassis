﻿using System;

namespace Chassis.Domain.Exceptions
{
    public class DuplicateProductException : Exception
    {
        public DuplicateProductException(string product)
            : base($"Product \"{product}\" is already in this order.")
        {
        }
    }
}
