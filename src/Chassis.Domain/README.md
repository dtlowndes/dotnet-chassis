﻿# Domain layer

Contains domain logic

- Domain Exceptions for violations at the Domain aggregate level
- ValueObjects (also supported by EF Core database migrations, see Repository layer)